﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    interface ISum<in T1>
    {
        int Summary(T1 value1, T1 value2);
    }
}
