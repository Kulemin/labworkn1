﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLab
{
    interface IDiff<out T>
    {
        T Difference (int value1, int value2);
    }
}
